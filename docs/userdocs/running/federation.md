#### Establishing the federation between clusters A -> B via admiralty

Create a service account in cluster B that you will run under:

`kubectl create serviceaccount sa`

Create the `source` in the same namespace:

```yaml
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Source
metadata:
  name: cluster-a
spec:
  serviceAccountName: sa
```

In the cluster A we need to create the secret holding credentials to access the cluster B:

```yaml
apiVersion: v1
data:
  config: <Base64-encoded config file to access the target cluster>
kind: Secret
metadata:
  name: cluster-b
type: Opaque
```

To create the config file, get the service account token from the corresponding secret. Then use the normal config file to access the cluster, but replace the user section with something similar to:

```yaml
users:
- name: default
  user:
    token: <Base64-decoded token>
```

Then we create the Target in cluster A, referencing the secret we just created:

```yaml
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Target
metadata:
  name: cluster-b
spec:
  kubeconfigSecret:
    name: cluster-b
```

After that we can label the namespace in cluster A as being federated:

`kubectl label ns fed-namespace multicluster-scheduler=enabled`

And try running a federated pod by adding the annotation:

```yaml
apiVersion: v1
kind: Pod
metadata:
  annotations:
    multicluster.admiralty.io/elect: ""
  name: test-pod
spec:
  containers:
  - name: mypod
    image: centos:centos7
    resources:
      limits:
        memory: 100Mi
        cpu: 100m
      requests:
        memory: 100Mi
        cpu: 100m
    command: ["sh", "-c", "echo 'Im a new pod' && sleep infinity"]
```
